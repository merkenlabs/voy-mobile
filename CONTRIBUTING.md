# ¡Toda contribución es bienvenida!

## Pasos para contribuir
1. Identifica el ticket en que quieres trabajar. Si no existe, crea uno!
1. Asigna el ticket a tu usuario.
1. Crea un branch.
1. Haz los cambios apropiados.
1. Commit, push
1. Crea un PR
1. ¡Listo!

Por ahora no tenemos mayores requisitos que estos. Solo esperamos aprender haciendo algo útil en el camino :)