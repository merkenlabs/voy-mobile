import 'Player.dart';

class Group {
  String name;
  String id;
  List<Player> participants;

  Group(String id, String name){
    this.name = name;
    this.id = id;
  }
}
