import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Scaffold is a layout for the major Material Components.
    return Scaffold(
      // body is the majority of the screen.
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[Icon(Icons.dashboard, size: 150.0, color: Colors.black12),
          Text('Acá irá el historial. Partidos pasados, estadísticas, etc.'),
          ],
        )
      ),
    );
  }
}