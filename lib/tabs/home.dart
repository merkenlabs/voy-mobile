import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(
    body: Center(
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            new Icon(
                Icons.home,
                size: 150.0,
                color: Colors.black12
            ),
            new Text('Acá van los próximos partidos (que son lo importante de ver en la app.)')
          ],
        )
    ),
    floatingActionButton: FloatingActionButton(
      tooltip: 'Add', // used by assistive technologies
      child: Icon(Icons.add),
      backgroundColor: Theme.of(context).primaryColor,
      onPressed: null,
    ),
  );
}