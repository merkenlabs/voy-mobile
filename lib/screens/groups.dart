import 'package:flutter/material.dart';
import 'package:voy_mobile/domain/Group.dart';

class Groups extends StatelessWidget {
  @override
  Widget build(BuildContext context) => new Scaffold(

      //App Bar
      appBar: new AppBar(
        title: new Text(
          'Grupos',
          style: new TextStyle(
            fontSize:
                Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),

      //Content of tabs
      body: ListView(
        children: <Widget>[
          _buildGroupCard(context, Group("Grupo uno", "123123123123")),
          _buildGroupCard(context, Group("Grupo dos", "123123123123")),
          _buildGroupCard(context, Group("Grupo tres", "123123123123")),
          _buildGroupCard(context, Group("Grupo cuatro", "123123123123")),
          _buildGroupCard(context, Group("Grupo cinco", "123123123123"))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add', // used by assistive technologies
        child: Icon(Icons.add),
        onPressed: null,
        backgroundColor: Theme.of(context).primaryColor,
      ));

  // #docregion Card
  Widget _buildGroupCard(BuildContext context, Group group) => SizedBox(
        height: 80,
        child: Card(
          elevation: 3,
          child: Column(
            children: [
              ListTile(
                title: Text(group.name,
                    style: TextStyle(fontWeight: FontWeight.w500)),
                subtitle: Text('Dato extra del grupo'),
                leading: Icon(
                  Icons.group,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ],
          ),
        ),
      );
// #enddocregion Card
}
